package com.cryptic.model.entity.combat.damagehandler.impl.sets;

import com.cryptic.model.World;
import com.cryptic.model.entity.Entity;
import com.cryptic.model.entity.combat.CombatType;
import com.cryptic.model.entity.combat.damagehandler.listener.DamageEffectListener;
import com.cryptic.model.entity.combat.formula.accuracy.MagicAccuracy;
import com.cryptic.model.entity.combat.formula.accuracy.MeleeAccuracy;
import com.cryptic.model.entity.combat.formula.accuracy.RangeAccuracy;
import com.cryptic.model.entity.combat.hit.Hit;
import com.cryptic.model.entity.player.Player;
import com.cryptic.model.items.container.equipment.Equipment;
import com.cryptic.model.items.container.equipment.EquipmentBonuses;
import com.cryptic.model.map.position.areas.impl.WildernessArea;

public class JusticiarSet implements DamageEffectListener {
    @Override
    public boolean prepareDamageEffectForAttacker(Entity entity, CombatType combatType, Hit hit) {
        var damage = hit.getDamage();
        if (entity instanceof Player player) {
            if (!WildernessArea.inWilderness(player.tile())) {
                if (damage > 0 && Equipment.justiciarSet(player)) {
                    EquipmentBonuses attackerBonus = player.getBonuses().totalBonuses(player, World.getWorld().equipmentInfo());
                    int bonus = attackerBonus.crushdef;
                    int formula = bonus / 3000;
                    damage = damage - formula;
                    damage = damage - 1;
                    hit.setDamage(damage);
                    return true;
                }
            }
        }
        return false;
    }
}
