package com.cryptic.model.content.raids.theatre.boss.verzik.phase;

public enum VerzikPhase {
    ONE,
    TWO,
    THREE,
    TRANSITIONING,
    DEAD;
}
