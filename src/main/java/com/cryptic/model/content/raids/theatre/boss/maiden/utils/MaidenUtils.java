package com.cryptic.model.content.raids.theatre.boss.maiden.utils;

import com.cryptic.model.entity.npc.NPC;
import com.cryptic.model.map.position.Area;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MaidenUtils {
    public static final Area IGNORED = new Area(3185, 4444, 3189, 4449);
    public static final Area MAIDEN_AREA = new Area(3160, 4435, 3187, 4458);

}
