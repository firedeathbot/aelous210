package com.cryptic.model.content.raids.theatre.loot;

public enum ChestType {
    DEFAULT,
    RARE_REWARD,
    DEFAULT_ARROW,
    RARE_REWARD_ARROW

}
