package com.cryptic.model.content.raids.theatre.stage;

public enum RoomState {
    INCOMPLETE,
    COMPLETE;
}
