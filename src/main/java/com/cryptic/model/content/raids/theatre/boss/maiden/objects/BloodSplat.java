package com.cryptic.model.content.raids.theatre.boss.maiden.objects;

import com.cryptic.model.map.object.GameObject;
import com.cryptic.model.map.position.Tile;

public class BloodSplat extends GameObject {
    public BloodSplat(int id, Tile tile, int type, int rotation) {
        super(id, tile, type, rotation);
    }
}
